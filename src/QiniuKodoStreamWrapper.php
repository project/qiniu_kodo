<?php
/**
 * Created by PhpStorm.
 * User: sosyuki
 * Date: 2018/2/27
 * Time: 21:10
 */

namespace Drupal\qiniu_kodo;


use Qiniu\Auth;
use Qiniu\Config;
use Qiniu\Storage\BucketManager;
use Qiniu\Storage\UploadManager;

class QiniuKodoStreamWrapper implements \DrupalStreamWrapperInterface {

  private $scheme = "qiniu";

  protected $uri;

  /**
   * @var array Default map for determining file mime types
   */
  protected static $mapping = NULL;

  /**
   * @var string
   */
  private $token;

  /**
   * @var int Current read/write position
   */
  protected $position = 0;

  /**
   * @var int Total size of the object as returned by OSS (Content-length)
   */
  protected $object_size = 0;

  /**
   * @var string Object read/write buffer, typically a file
   */
  protected $buffer = NULL;

  /**
   * @var int Buffer length
   */
  protected $buffer_length = 0;

  private $isPrivateBucket = FALSE;

  protected $domain;

  /**
   * @var array directory listing
   */
  protected $dir = [];

  /**
   * Flush the object buffers
   */
  protected function clearBuffer() {
    $this->position = 0;
    $this->object_size = 0;
    $this->buffer = NULL;
    $this->buffer_length = 0;
  }

  protected $auth;

  protected $bucket;

  protected $config;

  protected $bucketManager;

  protected $uploadManager;

  private $boot = FALSE;

  public function __construct() {
    // 需要填写你的 Access Key 和 Secret Key
    $accessKey = variable_get_value('qiniu_kodo_accessKey');
    $secretKey = variable_get_value('qiniu_kodo_secretKey');
    $this->bucket = variable_get_value('qiniu_kodo_bucket');
    $this->domain = variable_get_value('qiniu_kodo_domain');
    // 构建鉴权对象
    $this->auth = new Auth($accessKey, $secretKey);
    // 生成上传 Token
    $this->token = $this->auth->uploadToken($this->bucket);
    $this->config = new Config();
    $this->bucketManager = new BucketManager($this->auth, $this->config);
    $this->uploadManager = new UploadManager();
    $this->boot = TRUE;
  }


  protected function _is_dir($uri = NULL) {
    if ($uri == NULL) {
      $uri = $this->uri;
    }
    $path = $this->getLocalPath($uri);
    if (strlen($path) === 0) {
      return TRUE;
    }
    if (substr($uri, -1, 1) == '/') {
      return TRUE;
    }
    return FALSE;
  }

  protected function _format_response($uri, $response, $is_dir = FALSE) {
    $metadata = ['uri' => $uri];
    if (isset($response['fsize'])) {
      $metadata['fsize'] = $response['fsize'];
    }

    if (isset($response['mtime'])) {
      $metadata['mtime'] = $response['mtime'];
    }

    if (isset($response['ctime'])) {
      $metadata['ctime'] = $response['ctime'];
    }

    if ($is_dir) {
      $metadata['dir'] = 1;
      $metadata['mode'] = 0040000; // S_IFDIR indicating directory
      $metadata['mode'] |= 0777;
    }
    else {
      $metadata['dir'] = 0;
      $metadata['mode'] = 0100000; // S_IFREG indicating file
      $metadata['mode'] |= 0777; // everything is writeable
    }
    return $metadata;
  }

  protected function _get_object($uri) {
    if ($uri == "{$this->scheme}://" || $uri == "{$this->scheme}:") {
      $metadata = $this->_format_response('/', [], TRUE);
      return $metadata;
    }
    else {
      $uri = rtrim($uri, '/');
    }

    $is_dir = $this->_is_dir($uri);
    $metadata = NULL;
    if ($is_dir) {
      $metadata = $this->_format_response($uri, [], TRUE);
      return $metadata;
    }
    else {
      $localPath = $this->getLocalPath($uri);
      list($fileInfo, $err) = $this->bucketManager->stat($this->bucket, $localPath);
      if (empty($err)) {
        $metadata = $this->_format_response($uri, $fileInfo);
        return $metadata;
      }
      else {
        return FALSE;
      }
    }
  }

  public function setUri($uri) {
    $this->uri = $uri;
  }

  public function getUri() {
    return $this->uri;
  }

  public function getExternalUrl() {
    $path = explode('/', $this->getLocalPath());
    if ($path[0] == 'styles') {
      if (!$this->_get_object($this->uri)) {
        array_shift($path);
        return url('system/files/styles/' . implode('/', $path), ['absolute' => TRUE]);
      }
    }
    return $this->getUrl($this->getUri());
  }

  public static function getMimeType($uri, $mapping = NULL) {
    // Load the default file map
    if (!isset(self::$mapping)) {
      include_once DRUPAL_ROOT . '/includes/file.mimetypes.inc';
      self::$mapping = file_mimetype_mapping();
    }
    $extension = '';
    $file_parts = explode('.', basename($uri));
    // Remove the first part: a full filename should not match an extension.
    array_shift($file_parts);
    // Iterate over the file parts, trying to find a match.
    // For my.awesome.image.jpeg, we try:
    //   - jpeg
    //   - image.jpeg, and
    //   - awesome.image.jpeg

    while ($additional_part = array_pop($file_parts)) {
      $extension = strtolower($additional_part . ($extension ? '.' . $extension : ''));
      if (isset(self::$mapping['extensions'][$extension])) {
        return self::$mapping['mimetypes'][self::$mapping['extensions'][$extension]];
      }
    }

    return 'application/octet-stream';
  }

  public function chmod($mode) {
    return TRUE;
  }

  public function stream_open($uri, $mode, $options, &$opened_url) {
    $this->uri = $uri;
    if (strpbrk($mode, 'wax')) {
      $this->clearBuffer();
      return TRUE;
    }


    $metadata = $this->_get_object($uri);
    if ($metadata) {
      $this->clearBuffer();
      $this->object_size = $metadata['fsize'];
      return TRUE;
    }

    return FALSE;
  }

  protected function _stat($uri = NULL) {
    if (empty($uri)) {
      $uri = $this->getUri();
    }
    $isDir = $this->_is_dir($uri);

    list($metadata, $err) = $this->bucketManager->stat($this->bucket, $this->getLocalPath($uri));

    if ($metadata) {
      $stat = [];
      $stat[0] = $stat['dev'] = 0;
      $stat[1] = $stat['ino'] = 0;
      $stat[2] = $stat['mode'] = $isDir ? 0040000 : 0100000;
      $stat[3] = $stat['nlink'] = 0;
      $stat[4] = $stat['uid'] = 0;
      $stat[5] = $stat['gid'] = 0;
      $stat[6] = $stat['rdev'] = 0;
      $stat[7] = $stat['size'] = $metadata['fsize'];
      $stat[8] = $stat['atime'] = 0;
      $stat[9] = $stat['mtime'] = 0;
      $stat[10] = $stat['ctime'] = 0;
      $stat[11] = $stat['blksize'] = 0;
      $stat[12] = $stat['blocks'] = 0;

      if (!$isDir) {
        $stat[7] = $stat['size'] = isset($metadata['fsize']) ? $metadata['fsize'] : 0;
        $stat[8] = $stat['atime'] = isset($metadata['mtime']) ? $metadata['mtime'] : 0;
        $stat[9] = $stat['mtime'] = isset($metadata['mtime']) ? $metadata['mtime'] : 0;
        $stat[10] = $stat['ctime'] = isset($metadata['ctime']) ? $metadata['ctime'] : 0;
      }

      return $stat;
    }
    return FALSE;
  }

  public function stream_close() {
    $this->clearBuffer();
    return TRUE;
  }

  public function stream_lock($operation) {
    return FALSE;
  }

  public function stream_read($count) {
    try {
      // make sure that count doesn't exceed object size
      if ($count + $this->position > $this->object_size) {
        $count = $this->object_size - $this->position;
      }
      $data = '';
      if ($count > 0) {
        $range_end = $this->position + $count - 1;
        if ($range_end > $this->buffer_length) {
          $content = $this->_get_content($this->uri);
          $this->buffer .= $content;
          $this->buffer_length += strlen($content);
        }
        $data = substr($this->buffer, $this->position, $count);
        $this->position += strlen($data);
      }
      return $data;
    } catch (\Exception $e) {
      watchdog_exception(__FUNCTION__, $e);
      return NULL;
    }
  }

  /**
   * @todo 返回相对 bucket 的路径  不包含左侧  /
   *
   * @param $uri
   */
  protected function getLocalPath($uri = NULL) {
    if (!isset($uri)) {
      $uri = $this->uri;
    }
    $scheme = $this->scheme;
    $path = str_replace("{$scheme}://", '', $uri);
    $path = ltrim($path, '/');
    return $path;
  }

  protected function _get_content($uri) {
    $url = $this->getUrl($uri);
    return file_get_contents($url);
  }

  protected function getUrl($uri) {
    $localPath = $this->getLocalPath($uri);
    $url = 'http://' . $this->domain . '/' . $localPath;
    if ($this->isPrivateBucket) {
      $signedUrl = $this->auth->privateDownloadUrl($url);
      return $signedUrl;
    }
    return $url;
  }

  public function stream_write($data) {
    $data_length = strlen($data);
    $this->buffer .= $data;
    $this->buffer_length += $data_length;
    $this->position += $data_length;
    return $data_length;
  }

  public function stream_eof() {
    if (!$this->uri) {
      return TRUE;
    }
    return ($this->position >= $this->object_size);
  }

  public function stream_seek($offset, $whence) {
    switch ($whence) {
      case SEEK_CUR:
        // Set position to current location plus $offset
        $new_position = $this->position + $offset;
        break;
      case SEEK_END:
        // Set position to eof plus $offset
        $new_position = $this->object_size + $offset;
        break;
      case SEEK_SET:
      default:
        // Set position equal to $offset
        $new_position = $offset;
        break;
    }

    $ret = ($new_position >= 0 && $new_position <= $this->object_size);
    if ($ret) {
      $this->position = $new_position;
    }
    return $ret;
  }

  public function stream_flush() {
    if (empty($this->buffer)) {
      return TRUE;
    }

    $matadata = $this->_get_object($this->uri);
    if ($matadata) {
      $this->clearBuffer();
      return TRUE;
    }

    list($ret, $err) = $this->uploadManager->put($this->token, $this->getLocalPath(), $this->buffer);

    $this->clearBuffer();
    if (empty($err)) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  public function stream_tell() {
    return $this->position;
  }

  public function stream_stat() {
    return $this->_stat();
  }

  public function unlink($uri) {
    $this->assertConstructorCalled();
    if (!isset($uri)) {
      $uri = $this->uri;
    }
    $localPath = $this->getLocalPath($uri);
    $err = $this->bucketManager->delete($this->bucket, $localPath);
    if (empty($err)) {
      return TRUE;
    }
    return FALSE;
  }

  public function rename($from_uri, $to_uri) {
    $err = $this->bucketManager->move($this->bucket, $this->getLocalPath($from_uri),
      $this->bucket, $this->getLocalPath($to_uri), TRUE);
    if (empty($err)) {
      return TRUE;
    }
    return FALSE;
  }

  public function mkdir($uri, $mode, $options) {
    list($ret, $err) = $this->uploadManager->put($this->token, $this->getLocalPath($uri), NULL);
    if (empty($err)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * @param $uri
   * @param $options
   *
   * @return bool
   * 需要删除目录下的所有文件?
   */
  public function rmdir($uri, $options) {
    $err = $this->bucketManager->delete($this->bucket, $this->getLocalPath($uri));
    if (empty($err)) {
      return TRUE;
    }
    return FALSE;
  }

  public function url_stat($uri, $flags) {
    return $this->_stat($uri);
  }

  private function assertConstructorCalled() {
    if (!$this->boot) {
      $this->__construct();
    }
  }

  public function dir_opendir($uri, $options) {
    $this->assertConstructorCalled();
    if ($uri == NULL) {
      return FALSE;
    }
    return TRUE;
  }

  public function dir_readdir() {
    $filename = current($this->dir);
    if ($filename !== FALSE) {
      next($this->dir);
    }
    return $filename;
  }

  public function dir_rewinddir() {
    reset($this->dir);
    return TRUE;
  }

  public function dir_closedir() {
    $this->dir = [];
    return TRUE;
  }

  public function realpath() {
    return FALSE;
  }

  public function dirname($uri = NULL) {
    if (!isset($uri)) {
      $uri = $this->uri;
    }
    list($scheme, $target) = explode('://', $uri, 2);
    $target = trim($target, '\/');
    $dirname = dirname($target);
    if ($dirname == '.') {
      $dirname = '';
    }
    return $scheme . '://' . $dirname . '/';
  }


}